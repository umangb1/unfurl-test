FROM onecommons/unfurl:latest
WORKDIR .unfurl_home/
RUN unfurl --home . deploy 
RUN unfurl export > artifact-backup/deployment.json
CMD ["unfurl", "commit"]
